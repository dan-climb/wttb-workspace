import client, { getClient } from './sanity'

const pageFields = `
    _id,
    name,
    title,
    'slug': slug.current,
    hero
`

export async function getPageBySlug(slug: string) {
  const [data] = await getClient(true).fetch(
    `
        *[_type == "page" && slug.current == $slug] {
            ${pageFields}
        }
    `,
    { slug },
  )

  return data
}

export async function getPageSlugs() {
  const slugs = await client.fetch(`*[_type == "page"]{ 'slug': slug.current }`)
  return slugs.map(({ slug }) => ({ params: { slug: slug.split('/') } }))
}
