import type { IronSessionOptions } from 'iron-session'
import { withIronSessionApiRoute, withIronSessionSsr } from 'iron-session/next'
import {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  NextApiHandler,
} from 'next'
import { Prisma } from '@prisma/client'
const sessionOptions: IronSessionOptions = {
  password: process.env.SECRET_COOKIE_PASSWORD as string,
  cookieName: 'wttb-iron-session',
}

export function withSessionRoute(handler: NextApiHandler) {
  return withIronSessionApiRoute(handler, sessionOptions)
}

// Theses types are compatible with InferGetStaticPropsType https://nextjs.org/docs/basic-features/data-fetching#typescript-use-getstaticprops
export function withSessionSsr<
  P extends { [key: string]: unknown } = { [key: string]: unknown },
>(
  handler: (
    context: GetServerSidePropsContext,
  ) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>,
) {
  return withIronSessionSsr(handler, sessionOptions)
}

// This is where we specify the typings of req.session.*
declare module 'iron-session' {
  interface IronSessionData {
    user?: User
  }
}

type User = {
  accounts_email: string | null
  activation_code: string | null
  active: number
  average_monthly_spend: string | null
  billing_address_id: number
  birthday: string | null
  bpif: boolean
  brand_id: number
  charity: string
  charity_no: string | null
  company: string
  created_on: number
  credit_days: number
  credit_enabled: string
  credit_limit: Prisma.Decimal
  dashboard_widgets: string | null
  developer: string
  driver_user_id: number
  email: string
  fa_last_used_code: string | null
  fa_secret: string | null
  fan: string
  feedback: string | null
  first_name: string
  first_reminder: string
  forgotten_password_code: string | null
  forgotten_password_time: number | null
  id: number
  industry: string
  internal: number
  internal_invoice_emails: string
  invoice_email: string
  ip_address: string
  ipia: boolean
  is_legacy: boolean
  lapsed: string
  last_login: number
  last_name: string
  nickname: string | null
  no_membership: boolean
  old_user: string
  other: boolean
  other_specified: string
  profile_image: string | null
  remember_code: string | null
  reseller: boolean
  reseller_id: number
  reseller_rejected: boolean
  reseller_rejected_by: number | null
  reseller_rejected_on: Date | null
  reseller_verified: boolean
  salt: string | null
  second_reminder: string
  sector: string | null
  shipping_address_id: number
  show_reseller_markup_calculator: boolean
  site: string
  store_id: number | null
  telephone: string
  title: string
  tweak_user_token: string | null
  use_white_label: boolean
  username: string
  voucher_expires: Date
  voucher_id: number
  website: string | null
}
