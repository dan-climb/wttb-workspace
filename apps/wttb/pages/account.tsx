import { withSessionSsr } from '../lib/session'
import styled from 'styled-components'

import { InferGetServerSidePropsType } from 'next'

export default function Account({
  user,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (
    <Wrapper>
      <h1>
        {' '}
        Welcome {user.first_name} {user.last_name}
      </h1>
    </Wrapper>
  )
}

export const getServerSideProps = withSessionSsr(async function ({ req, res }) {
  const { user } = req.session

  if (!user) {
    res.setHeader('location', '/login')
    res.statusCode = 302
    res.end()
    return { props: { user: null } }
  }

  return { props: { user } }
})

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 5rem 1rem;
  width: 500px;
`
