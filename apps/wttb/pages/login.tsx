import { useForm } from 'react-hook-form'
import axios from 'axios'
import styled from 'styled-components'
import { useState } from 'react'
import { useRouter } from 'next/router'

export default function Login() {
  const router = useRouter()
  const [error, setError] = useState(null)
  const { register, handleSubmit } = useForm({
    defaultValues: { email: '', password: '' },
  })

  const onSubmit = handleSubmit(async data => {
    try {
      await axios.post('/api/login', data)
      router.push('/account')
    } catch (err) {
      setError('You have entered an invalid username or password')
    }
  })

  return (
    <Form onSubmit={onSubmit}>
      <input type="email" placeholder="email" {...register('email')} />
      <input type="password" placeholder="password" {...register('password')} />
      {error ? <p>{error}</p> : null}
      <button type="submit">Login</button>
    </Form>
  )
}

const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
  padding-top: 5rem;
  width: 500px;

  input {
    margin-bottom: 2rem;
    padding: 1rem;
  }

  button {
    padding: 1rem;
    width: fit-content;
    margin-left: auto;
    color: white;
    background-color: coral;
    border: none;
  }
`
