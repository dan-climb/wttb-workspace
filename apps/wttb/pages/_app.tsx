import Head from 'next/head'
import { createGlobalStyle, ThemeProvider } from 'styled-components'

const App = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <title>Where the trade buys</title>
        <meta name="robots" content="noindex" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  )
}

export default App

const theme = {
  colors: {
    primary: '#e985b3',
    primaryDark: '#b5507f',
    secondary: '#a9bfd3',
    secondaryDark: '#485c6e',
    tertiary: '#f2da8e',
    black: '#595C64',
    greyDark: '#9c9b9b',
    greyLight: '#e4e4e4',
    white: '#ffffff',
  },
  breakpoints: {
    mobile: `@media screen and (min-width: 425px)`,
    tablet: `@media screen and (min-width: 768px)`,
    laptop: `@media screen and (min-width: 1024px)`,
    desktop: `@media screen and (min-width: 1200px)`,
  },
}

const globalFont = 'Arial'

const GlobalStyle = createGlobalStyle`
  html {
    font-family: ${globalFont}, sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }
  body {
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`
