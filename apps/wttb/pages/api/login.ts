import { withSessionRoute } from '../../lib/session'
import prisma from '../../lib/prisma'
import { NextApiRequest, NextApiResponse } from 'next'
import { compare } from 'bcryptjs'

export default withSessionRoute(async function loginRoute(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { email, password } = req.body

  const user = await prisma.users.findFirst({
    where: { email },
  })

  if (!user) {
    return res
      .status(404)
      .json({ message: 'You have entered an invalid username or password' })
  }

  const match = await compare(password, user.password)

  if (!match) {
    return res
      .status(404)
      .json({ message: 'You have entered an invalid username or password' })
  }

  // Pull off password hash so it isn't exposed
  const { password: _password, ...rest } = user

  req.session.user = rest
  await req.session.save()
  return res.status(200).json(rest)
})
