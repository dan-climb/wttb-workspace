import { GetStaticPaths, GetStaticProps } from 'next'
import styled from 'styled-components'
import Hero from '../components/hero'
import { getPageBySlug, getPageSlugs } from '../lib/api'

export default function Home({ page }) {
  return (
    <Main>
      <Hero {...page.hero} />
    </Main>
  )
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getPageSlugs()

  return {
    paths,
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps = async ({
  params,
  preview = false,
}) => {
  const slug =
    typeof params?.slug === 'string' ? params.slug : params?.slug?.join('/')

  const page = await getPageBySlug(slug)

  return { props: { page } }
}

const Main = styled.main`
  display: grid;
  place-items: center;
  width: 100%;
  height: 100vh;
`
