import { GetStaticPropsContext } from 'next'
import styled from 'styled-components'
import Hero from '../components/hero'
import { getPageBySlug } from '../lib/api'

export default function Home({ page }) {
  return (
    <Main>
      <Hero {...page.hero} />
    </Main>
  )
}

export async function getStaticProps({
  params,
  preview = false,
}: GetStaticPropsContext) {
  const page = await getPageBySlug('/')

  return { props: { page } }
}

const Main = styled.main`
  display: grid;
  place-items: center;
  width: 100%;
  height: 100vh;
`
