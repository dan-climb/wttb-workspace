// https://www.npmjs.com/package/next-transpile-modules
const withTM = require('next-transpile-modules')(['@wttb/content'])

module.exports = withTM({
  images: {
    domains: ['cdn.sanity.io'],
  },
})
