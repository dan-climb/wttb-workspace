import Image from 'next/image'
import { imageBuilder } from '../lib/sanity'

export default function CoverImage({ title, imageObject }) {
  return (
    <Image
      width={1240}
      height={540}
      alt={`Cover Image for ${title}`}
      src={imageBuilder(imageObject).width(1240).height(540).url()}
    />
  )
}
