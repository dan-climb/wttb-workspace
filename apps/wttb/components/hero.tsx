import PortableText from 'react-portable-text'
import CoverImage from './coverImage'

export default function Hero({ title, tagline, backgroundImage }) {
  return (
    <div>
      <CoverImage title={title} imageObject={backgroundImage} />
      <h1>{title}</h1>
      <PortableText className={null} content={tagline} serializers={{}} />
    </div>
  )
}
