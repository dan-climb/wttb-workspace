import { RiPagesFill as icon } from 'react-icons/ri'

export default {
  name: 'page',
  title: 'Page',
  type: 'document',
  icon,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 100,
      },
    },
    {
      name: 'hero',
      title: 'Hero',
      type: 'hero',
    },
  ],
  preview: {
    select: {
      title: 'title',
      tagline: 'hero.tagline',
      media: 'hero.backgroundImage',
    },
    prepare({ tagline, ...rest }) {
      const [{ children }] = tagline
      const text = children.map(({ text }) => text).join('')

      return { subtitle: text, ...rest }
    },
  },
}
