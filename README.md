# WTTB Workspace

This project is built using yarn workspaces to keep different apps close
together and have shared components or utils whilst still keeping them separate.

It is only using yarn workspaces rather than making use of Lerna or using Nx
because the project does not care about sharing lots of components with the risk
of breaking changes in different places so we do not have to version packages
and Nx is just too boilerplatey and opinionated.

This also works really well thanks to
[next-transpile-modules](https://github.com/martpie/next-transpile-modules)
which can transpile code from our local shared packages allowing for hot module
reloading from changes to the shared package.

## Getting started

So far we are making use of a few tools that give us a lot of functionality but
also have a little bit extra for getting up and running.

To get started run `yarn bootstrap` in the root of the monorepo which will
install packages for all folders in the workspace and will generate your prisma
client.

Running `yarn start` in the root will spin up both the wttb app and the cms
concurrently. If required the apps can be started in isolation by running
`yarn dev` for the app or `yarn cms` for the cms.

## Development

Whilst working on this project there are a few dependencies that come into play
as external bits and bobs change.

- Changes to database - If the database structure changes in anyway the schema
  can be updated by running `npx prisma db pull` this should be followed by
  `npx prisma generate` to update the prisma client.

- Change to prisma schema - If the schema changes due to a `prisma db pull` or
  from a manual update you should run `npx prisma generate` to update the client

- Changes to CMS schemas - After making updates to the schemas in `apps/cms` you
  should run `yarn codegen` in the root of the monorepo which will create/update
  any types for use in `apps/wttb`
